// Script to Download Training Images and Annotations from Google's Open Images Database.
// This script is base on this python script : https://github.com/spmallick/learnopencv/blob/master/YOLOv3-Training-Snowman-Detector/getDataFromOpenImages_snowman.py
// see the original readme here : https://github.com/spmallick/learnopencv/tree/master/YOLOv3-Training-Snowman-Detector
// Node version written by Moe Alam, Shinobi Systems
process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err);
});
var fs = require('fs')
var execSync = require('child_process').execSync
var spawn = require('child_process').spawn

var runMode = "train"

var classes = [
    "Handgun"
]

if(process.argv[2]){
    classes = process.argv[2].split(',')
}

var joinedClassNames = classes.join('').replace(/ /g,'')
var classesNamesFile = __dirname + '/classes.names'
// if(!fs.existsSync(classesNamesFile)){
    classes.forEach(function(dClass,key){
        var pushOperator = '>>'
        if(key === 0)pushOperator = '>'
        execSync(`echo "${dClass}" ${pushOperator} ${classesNamesFile}`)

    })
// }
var trainTxtFilename = __dirname + `/${joinedClassNames}_train.txt`
var testTxtFilename = __dirname + `/${joinedClassNames}_test.txt`
var darknetDataFilename = 'darknet.data'
// if(!fs.existsSync(darknetDataFilename)){
        fs.unlink(trainTxtFilename,() => {})
        fs.unlink(testTxtFilename,() => {})
        console.log('creating ' + darknetDataFilename)
        execSync(`echo "classes = ${classes.length}" > ${darknetDataFilename}`)
        execSync(`echo "train  = ${trainTxtFilename}" >> ${darknetDataFilename}`)
        execSync(`echo "valid  = ${testTxtFilename}" >> ${darknetDataFilename}`)
        execSync(`echo "names = ${classesNamesFile}" >> ${darknetDataFilename}`)
        execSync(`echo "backup = weights" >> ${darknetDataFilename}`)
        try{
            fs.mkdirSync('weights')
        }catch(err){}
// }

var classDescriptionBoxableCsvFilename = `class-descriptions-boxable.csv`
var classDescriptionBoxableCsvUrl = `https://storage.googleapis.com/openimages/2018_04/${classDescriptionBoxableCsvFilename}`
var trainAnnotationsBboxCsvFilename = `train-annotations-bbox.csv`
var trainAnnotationsBboxCsvUrl = `https://storage.googleapis.com/openimages/2018_04/train/${trainAnnotationsBboxCsvFilename}`

if(!fs.existsSync(classDescriptionBoxableCsvFilename)){
    exec(`wget ${classDescriptionBoxableCsvUrl}`)
}
if(!fs.existsSync(trainAnnotationsBboxCsvFilename)){
    exec(`wget ${trainAnnotationsBboxCsvUrl}`)
}

var classDescriptionBoxableCsv = fs.readFileSync(classDescriptionBoxableCsvFilename,'utf8').split('\n')
var classDescriptionBoxableJson = {}
classDescriptionBoxableCsv.forEach(function(row){
    var split = row.split(',')
    if(split[1] && split[0])classDescriptionBoxableJson[split[1]] = split[0]
})

var jpegImagesFolder = __dirname + '/JPEGImages/'
var labelsFolder = __dirname + '/labels/'

if(!fs.existsSync(jpegImagesFolder)){
    fs.mkdirSync(jpegImagesFolder)
}
if(!fs.existsSync(labelsFolder)){
    fs.mkdirSync(labelsFolder)
}

classes.forEach(function(className,ind){
    console.log(`Class ${ind} : ${className}`)
    var commandStr = `grep "${classDescriptionBoxableJson[className]}" ${runMode}-annotations-bbox.csv`
    console.log(commandStr)
    var classAnnotations = execSync(commandStr,'utf8').toString().split(/\r?\n/)
    var totalNumOfAnnotations = classAnnotations.length
    console.log(`Total number of annotations : ${totalNumOfAnnotations}`)
    var cnt = 0
    classAnnotations.forEach(function(line){
        ++cnt
        console.log(`${className} : ${cnt} / ${totalNumOfAnnotations}`)
        var lineParts = line.split(',')
        if(!lineParts[2])return;
        if(!fs.existsSync(`${jpegImagesFolder}${lineParts[0]}.jpg`)){
            execSync(`wget https://open-images-dataset.s3.amazonaws.com/${runMode}/${lineParts[0]}.jpg -O ${jpegImagesFolder}${lineParts[0]}.jpg`)
        }
        var annotationContents = [
            ind.toString(),
            ((parseFloat(lineParts[5]) + parseFloat(lineParts[4]))/2).toString(),
            ((parseFloat(lineParts[7]) + parseFloat(lineParts[6]))/2).toString(),
            (parseFloat(lineParts[5]) - parseFloat(lineParts[4])).toString(),
            (parseFloat(lineParts[7]) - parseFloat(lineParts[6])).toString()
        ].join(' ') + '\n'
        var labelName = `labels/${lineParts[0]}.txt`
        if(fs.existsSync(labelName) && fs.readFileSync(labelName).indexOf(annotationContents) > -1){
            return
        }
        fs.appendFileSync(labelName,annotationContents)
    })
})
