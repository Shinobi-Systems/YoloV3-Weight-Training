// Script to Download Training Images and Annotations from Google's Open Images Database.
// This script is base on this python script : https://github.com/spmallick/learnopencv/blob/master/YOLOv3-Training-Snowman-Detector/getDataFromOpenImages_snowman.py
// see the original readme here : https://github.com/spmallick/learnopencv/tree/master/YOLOv3-Training-Snowman-Detector
// Node version written by Moe Alam, Shinobi Systems

var fs = require('fs')
var execSync = require('child_process').execSync
var spawn = require('child_process').spawn

var classesNamesFile = 'classes.names'
var joinedClassNames = fs.readFileSync(classesNamesFile,'utf8').split('\n').join('').replace(/ /g,'')
var jpegImagesFolder = 'JPEGImages/'
var trainTxtFilename = `${joinedClassNames}_train.txt`
var testTxtFilename = `${joinedClassNames}_test.txt`
fs.unlink(trainTxtFilename,() => {})
fs.unlink(testTxtFilename,() => {})
var files = fs.readdirSync(jpegImagesFolder)
var testLength = parseInt(files.length * 0.1)
var testArray = []
for (i = 0; i < testLength; i++) {
    var randomKey = Math.floor(Math.random() * files.length)
    testArray.push(randomKey)
}
var trainTxtContents = []
var testTxtContents = []
files.forEach(function(filename,key){
    var fileRow = jpegImagesFolder + filename
    if(testArray.indexOf(key) > -1){
        testTxtContents.push(fileRow)
    }else{
        trainTxtContents.push(fileRow)
    }
})
fs.writeFileSync(trainTxtFilename,trainTxtContents.join('\n'),'utf8')
fs.writeFileSync(testTxtFilename,testTxtContents.join('\n'),'utf8')
